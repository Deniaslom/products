package service;

import beans.ClientCard;
import beans.Product;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import utils.GenerationClientsCardDB;
import utils.GenerationProductsDB;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * This is a service class that implements communication
 * with the database (in this case, with a list of products and a list of cards)
 */
public class Purchase {
    private static final Logger LOGGER = Logger.getLogger(service.Purchase.class);

    public Purchase() {
    }

    /**
     * @param str Input parameter indicating the number of products
     * @return list of products from the database
     */
    public List<Product> findProductsFromDB(String str){
        Pattern pattern1 = Pattern.compile("(([1-9]+)\\-([1-9]+))+");
        Matcher matcher1 = pattern1.matcher(str);
        List<Product> products = new ArrayList<>();


        while(matcher1.find()){

            Pattern pattern2 = Pattern.compile("[1-9]+");
            Matcher matcher2 = pattern2.matcher(matcher1.group());

            if(matcher2.find()) {
                Product product = GenerationProductsDB.getInstance().getProducts().
                        stream().
                        filter(o -> o.getId() == Integer.parseInt(matcher2.group())).
                        findFirst().get();
                if(product != null){
                    products.add(product);
                }
            }
        }
        products.stream().forEach(System.out :: println);
        return products;

    }

    /**
     * @param str an input parameter containing a discount card
     * @return returns a card from the database
     */
    public ClientCard findClientCart(String str) {
        Pattern pattern1 = Pattern.compile("card\\-[1-9]+");
        Matcher matcher1 = pattern1.matcher(str);
        ClientCard card = null;
        boolean flag = true;
        while (matcher1.find() && flag){
            Pattern pattern2 = Pattern.compile("[1-9]+");
            Matcher matcher2 = pattern2.matcher(matcher1.group());
            while(matcher2.find() && flag){
                try{
                    card = GenerationClientsCardDB.getInstance().getClientCards().
                            stream().
                            filter(o -> o.getId() == Integer.parseInt(matcher2.group())).
                            findFirst().get();
                } catch (Exception e){
                    LOGGER.log(Level.ERROR, "cart not found: " + e);
                }

                if(card != null){
                    flag = false;
                }
            }
        }
        System.out.println("card: " + card);

        return card;
    }

    /**
     * @param str input string containing products and customer card
     * @return total cost
     */
    public double totalCost(String str){
        List<Product> products = findProductsFromDB(str);
        ClientCard cart = findClientCart(str);
        double dis = 0;
        double totalCost = 0;
        if(cart != null){
            dis = cart.getDiscount();
        }
        for(Product product : products){
            totalCost += (product.totalCost()*(100-dis)/100);
        }

        System.out.println("total cost: " + totalCost);

        return totalCost;
    }

    public void checkOut(String str){
        System.out.printf("%-5s%-11s%-10s%-10s%-10s%n", "id", "name", "price", "count", "cost");
        totalCost(str);
    }

}
