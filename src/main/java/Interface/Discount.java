package Interface;

public interface Discount {

    double totalCost();
}
