package beans;

public class ProductWithDiscount extends Product{
    public ProductWithDiscount() {
    }

    @Override
    public double totalCost() {
        if (getCount() > 5) {
            return getPrice() * getCount() * 0.9;
        }
        return getPrice() * getCount();
    }

    @Override
    public String toString() {
        return String.format("%-5d%-11s%.3f%10d%10.3f", getId(), getName(), getPrice(), getCount(), totalCost());
    }
}
