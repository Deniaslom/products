package beans;

public class ProductWithoutDiscount extends Product{
    public ProductWithoutDiscount() {
    }

    @Override
    public double totalCost() {
        return getCount()*getPrice();
    }

    @Override
    public String toString() {
        return String.format("%-5d%-11s%.3f%10d%10.3f", getId(), getName(), getPrice(), getCount(), totalCost());
    }


}
